/*
	Do the following in the terminal after creating the index.js
	
	- Create a "package.json" file
		npm init -y 

	- Install Express JS and Mongoose.
		npm install express mongoose

*/
// Include express module
const express = require("express");

//Mongoose is a package that allows creation of Schemas to model our data structure.
//Also has access to a number of methods for manipulation out database.
const mongoose = require("mongoose");
//create a server(set up express server in app variable)
const app = express();

//List the port where the server will listen
const port = 3001;

//MongoDB Connection
//Connect to the database by passing in your connection string.

// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application.
// To prevent/avoid any current and future errors while connecting to Mongo DB, add the userNewUrlParser and useUnifiedTopology.

/*
	Syntax:
	mongoose.connect("<MongoDB Atlas Connection String>", {userNewUrlParser: true, useUnifiedTopology: true});
*/
mongoose.connect("mongodb+srv://admin:admin@cluster0.mmogcml.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our database.
let db = mongoose.connection;

//If a connection error occurred, it will be output in the console.
//console.error.bind(console) allows us to print the error in the browser consoles and in the terminal.
db.on("error", console.error.bind(console, "connection error"));

//If the connection is successful, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud database."));


//Mongoose Schema
//Determine the structure of the documents to be written in the database (para malaman pano ma save sa database)
//Schemas act as blueprint to out data.

/*

	Syntax:
		const schemaName = new mongoose.Schema({
			propertyA:valueA,
			propertyB:valueB
		})

*/

const taskSchema = new mongoose.Schema({
	//no need id, is auto generated
	//Name of the task
	name: String, //name: String is a shorthand for name:{type: String}(if wla na any other values) 
	//Status task(Complete, Pending, Incomplete)
	status: {
		type: String,
		//Default values are the predefined(established in advance) values for a field if we don't put any value.
		default: "Pending" 
	}
})

//Mongooose Model 
//Model uses Schemas and they act as the middleman from the server (JS code) to our database.

/*
	Syntax:
	Const modelName = mongoose.model("collectionName", mongooseSchema);
*/
	// Model must be singular form and capitalize the first letter.
	// The variable/object "Task" can now used to run commands for interacting with our database
	// "Task" is both capitalized following the MVC approach for naming conventions
	// Models must be in singular form and capitalized
	// The first parameter of the Mongoose model method indicates the collection in where to store the data
	// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
	
	// Using mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection.// meaning sa mondoDB kay tasks na with "s" plural na cxa.
	// model name is task
	const Task = mongoose.model("Task", taskSchema);

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//to create a collection inside mongoDB
//create a Route for creating a task

// Route for creating a task
//Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is not duplicated if:
		- result from the query not equal to null.
		- req.body.name(ddto sa robo t-3) is equal to result.name.(result name pag play sa robo t-3)
*/

//creating a task gagamitin si server app then use Post http method
app.post("/tasks", (req, res)=>{
	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria
	//Call back functions in mongoose method are programmed this way:
		//first parameter store the error(err)
		//secong parameter return the result.(result)
		//req.body.name = eat
		//Task.findOne({name:eat})
	Task.findOne({name:req.body.name}, (err, result) =>{
		console.log(result);

		//If a docuemnt was found and the docuemnt's name matches the infromation sent by the client/postman
		if(result != null && req.body.name == result.name){
			//Return a messaage to the client/postman
			return res.send("Duplicate task found!");
		}
		//If no document was found or no duplicate
		else{
			//Create a new task object and save to the database
			let newTask = new Task({
				name: req.body.name
			});

			// The "save" method will store the information to the database
			//Since the "newTask" was created/ instatiated from the Task model that contain the Mongoose Schema, so it will gain access to the save method above.

			//.save parang push method pero pag mag update pwede mo rin gamitin
			newTask.save((saveErr, saveTask) =>{
				
				//If there are errors in saving, it will be displayed in the console
				if(saveErr){
					return console.error(savaeErr); //this is in object format
				}
				//IF no error found while creating the document it will be save in the database.
				else{
					return res.status(201).send("New task created.");

				/* after this go to postman {
   											 "name": "write here anytask" 
											 }                  */	
				}
			})
		}
	})

})

//para masave at di na mag check sa mongodb

// Business Logic
		/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client/Postman and return an array of documents
		*/
		
app.get("/tasks", (req,res)=>{
	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result); 
		}
	})
})




//Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.
*/

// Create a User Schema
const userSchema = new mongoose.Schema({
	// Name of the task
	username: String, //name: String is a shorthand for name:{type: String}
	// Status task (Complete, Pending, Incomplete)
	password: String
	
})

// Create a User Model
//.model- para malaman yung anong format
const User = mongoose.model("User", userSchema);



//Register a user

/*
	Business Logic:

	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
		- Note: Make sure that the username and password from the request is not empty.
	3. Create a new User object with a "username" and "password" fields/properties
*/

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) =>{
		// Check for duplicates
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found!")
		}
		// No duplicate
		else{
			if(req.body.username != "" && req.body.password != ""){
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});

				newUser.save((saveErr, saveUser) =>{
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return res.status(201).send("New User Registered!");
					}
				})
			}
			else{
				return res.send("Both username and password must be provided.");
			}
		}
	})
})

		
app.get("/signup", (req,res)=>{
	
	User.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result); 
		}
	})
})




// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));

